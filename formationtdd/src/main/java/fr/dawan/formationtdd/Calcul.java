package fr.dawan.formationtdd;

public class Calcul {

    public int addition(int a, int b) {
        return a + b;
    }

    public double division(double i, double j) {
        if (j == 0.0) {
            throw new ArithmeticException();
        }
        return i / j;
    }

}
