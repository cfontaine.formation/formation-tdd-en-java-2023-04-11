package fr.dawan.formationtdd;

import java.awt.Dimension;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import fr.dawan.formationtdd.beans.Personne;

public class Exemple {

    public static double[] processArray() {
        double t[] = { 4.5, 6.0, 5.7, 3.4, 100.0000001 };
        return t;
    }

    public static List<Integer> processList() {
        return Arrays.asList(1, 6, 4, 23);
    }

    public static Map<Integer, String> processMap() {
        Map<Integer, String> m = new HashMap<>();
        m.put(12, "azerty");
        m.put(34, "Hello");
        m.put(42, "Answer");
        return m;
    }

    public static void traitement() throws InterruptedException {
        Thread.sleep(50);
    }

    public static boolean interval(int val, int min, int max) {
        return val >= min && val <= max;
    }

    public static Dimension createDimension() {
        return new Dimension(600, 400);
    }

    public static Personne createPersonne() {
        Personne p = new Personne("yves", "roulot", LocalDate.of(1988, 5, 14));
        p.setId(3L);

        return p;
    }

    public static String createString() {
        return "Hello World";
    }

    public static String createJson() throws JsonProcessingException {
        Personne p = new Personne("John", "Doe", LocalDate.of(1991, 11, 9));
        p.setId(5L);
        // Serialisation en json avec jackson
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule()); // Ajouter le support de java.time.*
        return mapper.writeValueAsString(p);
    }

    
    public static String createArrayJson() throws JsonProcessingException {
        List<Personne> lstPersonne=new ArrayList<>();
        Personne p=new Personne("John", "Doe", LocalDate.of(1991, 11, 9));
        p.setId(5L);
        lstPersonne.add(p);
        p=new Personne("Jane", "Doe", LocalDate.of(1996, 9, 11));
        p.setId(6L);
        lstPersonne.add(p);
        
        // Serialisation en json avec jackson
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule()); // Ajouter le support de java.time.*
        return mapper.writeValueAsString(lstPersonne);
    }
    
    public static LocalDate lastDayOfMonth(Month month) {
        switch(month) {
            case JANUARY:
            case MARCH:
            case MAY:
            case JULY:
            case AUGUST:
            case OCTOBER:
            case DECEMBER:
                return LocalDate.of(2023, month, 31);
            case FEBRUARY:
                return LocalDate.of(2023, month, 28);
            default:
                return LocalDate.of(2023, month, 30);
        }
    }
}
