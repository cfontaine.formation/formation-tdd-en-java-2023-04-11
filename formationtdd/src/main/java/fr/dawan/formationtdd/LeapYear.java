package fr.dawan.formationtdd;

public class LeapYear {

    public boolean isLeapYear(int i) {
        
        return estDivisible(i, 4) && !estDivisible(i, 100) || estDivisible(i, 400);
    }
    
    private boolean estDivisible(int nombre, int diviseur)
    {
        return nombre % diviseur==0;
    }

}
