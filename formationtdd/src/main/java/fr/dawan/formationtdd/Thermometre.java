package fr.dawan.formationtdd;

public class Thermometre {

    public int nearestZero(int ... temperature) throws Exception {
        if(temperature.length==0) {
            return 0;
        } else if(temperature.length>100) {
            throw new Exception("Nombre de temperature> à 100");
        }
        int tnz=temperature[0];
        for(int t:temperature) {
            if(Math.abs(t)<Math.abs(tnz)) {
                tnz=t;
            }
            if(tnz<0 && Math.abs(t)==Math.abs(tnz)) {
                tnz=t;
            }
        }
        return tnz;
    }

}
