package fr.dawan.formationtdd.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.dawan.formationtdd.beans.Article;

public class ArticleRepository {

    private static Map<Long, Article> articles = new HashMap<>();

    private long lastId = 4L;

    static {
        Article a = new Article("Souris", 19.0);
        a.setId(1L);
        articles.put(1L, a);
        a = new Article("Clavier", 29.0);
        a.setId(2L);
        articles.put(2L, a);
        a = new Article("Cles USB 64Go", 30.0);
        a.setId(3L);
        articles.put(3L, a);
    }

    public void saveOrUpdate(Article a) {
        if (a.getId() == 0L) {
            a.setId(lastId);
            articles.put(lastId++, a);
        } else {
            articles.put(a.getId(), a);
        }

    }

    public void delete(Article a) throws Exception {
        Article ar = articles.remove(a.getId());
        if (ar == null) {
            throw new Exception();
        }
    }

    public void delete(long id) throws Exception {
        Article ar = articles.remove(id);
        if (ar == null) {
            throw new Exception();
        }
    }

    public Article findById(long id) throws Exception {
        Article a = articles.get(id);
        if (a == null) {
            throw new Exception();
        }
        return a;
    }

    public List<Article> findAll() {
        return articles.values().stream().toList();
    }
    
    public static Article tool() {
        return new Article();
    }

}
