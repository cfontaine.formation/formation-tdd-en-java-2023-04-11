package fr.dawan.formationtdd.services;

import java.util.List;

import fr.dawan.formationtdd.beans.Article;
import fr.dawan.formationtdd.repository.ArticleRepository;

public class ArticleService {

    private ArticleRepository repository;

    public ArticleService(ArticleRepository repository) {
        this.repository = repository;
    }

    public void save(Article a) {
        repository.saveOrUpdate(null);
    }

    public void delete(Article a) throws Exception{
            repository.delete(a);

    }
    public void delete(long id) throws Exception{
        repository.delete(id);

}

    public List<Article> findAll() {
        return repository.findAll();
    }

    public Article findById(long id) throws Exception{
        return repository.findById(id);
    }
    
    public static Article check() {
        return ArticleRepository.tool();
    }
}
