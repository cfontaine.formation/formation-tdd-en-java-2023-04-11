package fr.dawan.formationtdd;

import static org.hamcrest.MatcherAssert.assertThat;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.dawan.formationtdd.beans.Article;
import fr.dawan.formationtdd.beans.Personne;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class ArticleHibernateTest {
    private static EntityManagerFactory emf;
    
    private static EntityManager em;
    
    @BeforeAll
    public static void setup() {
        emf=Persistence.createEntityManagerFactory("formationtdd");
    }
    
    @BeforeEach
    public void init() {
        em=emf.createEntityManager();
    }
    
    @Test
    public void articleTest() {
        Article expected=new Article("Souris",19.0);
        expected.setId(1L);
        assertThat(expected, Matchers.samePropertyValuesAs(em.find(Article.class,1L))); 
    }
    
    @AfterEach
    public void destroy() {
        em.close();
    }
    
}
