package fr.dawan.formationtdd;

import java.sql.Connection;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.dbunit.DataSourceBasedDBTestCase;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.h2.jdbcx.JdbcDataSource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class DataSourceDbunitTest extends DataSourceBasedDBTestCase {

    private Connection cnx;

    @Override
    protected DataSource getDataSource() {
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setUrl(
                "jdbc:h2:mem:articleTest;DB_CLOSE_ON_EXIT=FALSE;INIT=runscript from 'classpath:database.schema'");
        dataSource.setUser("sa");
        dataSource.setPassword("");
        return dataSource;
    }

    @Override
    protected IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(getClass().getClassLoader().getResourceAsStream("data.xml"));
    }

    @Override
    protected DatabaseOperation getSetUpOperation() throws Exception {
        return DatabaseOperation.REFRESH;
    }

    @Override
    protected DatabaseOperation getTearDownOperation() throws Exception {
        return DatabaseOperation.DELETE_ALL;
    }

    @Before
    public void setUp() throws Exception {
        cnx = getConnection().getConnection();
    }

    @Test
    public void articleTest() throws Exception {
        assertNotNull(cnx);
        ResultSet rs = cnx.createStatement().executeQuery("SELECT id,description,prix FROM articles WHERE id=1");
        assertTrue(rs.next());
        assertEquals("Souris", rs.getString("description"));
        assertEquals(19.0, rs.getDouble("prix"));
    }

}
