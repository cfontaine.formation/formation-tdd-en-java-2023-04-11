package fr.dawan.formationtdd;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assumptions.assumeFalse;
import static org.junit.jupiter.api.Assumptions.assumingThat;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

import java.io.File;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;
import org.json.JSONException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.EnumSource.Mode;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.ArraySizeComparator;

import com.fasterxml.jackson.core.JsonProcessingException;

import fr.dawan.formationtdd.beans.Personne;
//@TestInstance(Lifecycle.PER_CLASS)
public class ExempleTest {
    
//    @BeforeAll
//    public void setup() {
//        
//    }
    
    public ExempleTest() {
        System.out.println("Constructeur ExempleTest");
    }

    // Exemple d'assertion de Junit
    @Test
    @DisplayName("Assertion égalité tableau")
    public void arrayTest() {
        double[] expected = { 4.5, 6.0, 5.7, 3.4, 100.0 };
        double[] resultat = Exemple.processArray();
        assertArrayEquals(expected, resultat, 0.00001);
    }

    @Test
    @DisplayName("Assertion égalité collection")
    public void iterableTest() {
        List<Integer> expected = Arrays.asList(1, 6, 4, 23);
        assertIterableEquals(expected, Exemple.processList());
    }

    @Test
    @DisplayName("Assertion durée d'execution")
    public void timoutTest() {
        // assertTimeout(Duration.ofMillis(200), () -> Exemple.traitement());
        assertTimeoutPreemptively(Duration.ofMillis(200), () -> Exemple.traitement());
        // => assertTimeoutPreemptively arréte le test une fois la durée définie par
        // l'assertion est dépasseé
    }

    // Message dans les assertions (dernier paramètre)
    @Test
    @DisplayName("Messsage dans les assertions")
    @Disabled
    public void messageAssertionTest() {
        int sut = 3;
        assertEquals(4, sut, "Message de l'assertion");
    }

    // Nombre d'assertion par méthode
    // – tester un seul concept par méthode de test
    // - minimiser le nombre d'assertion par concept
    @Test
    public void intervalLimitMinTest() {
        assertTrue(Exemple.interval(3, -4, 7));
        assertTrue(Exemple.interval(-5, -5, 7));
        assertFalse(Exemple.interval(-6, -4, 7));
    }

    @Test
    public void intervalLimitMaxTest() {
        assertTrue(Exemple.interval(3, -4, 7));
        assertTrue(Exemple.interval(7, -5, 7));
        assertFalse(Exemple.interval(8, -4, 7));
    }

    @Test
    @Disabled
    public void checkDimensionTest() {
        assertEquals(300, Exemple.createDimension().width);
        // La deuxième assertion n'est pas éxécuté car la première a échoué
        assertEquals(200, Exemple.createDimension().height);
    }

    // Regrouper les assertions => assertAll
    // permet de regrouper plusieurs assertions qui seront toutes exécutées
    // même si une des assertions du groupe échoue

    @Test
    @Disabled
    void checkAllDimensionTes() {
        assertAll("Dimension non conforme", () -> assertEquals(300, Exemple.createDimension().width),
                () -> assertEquals(200, Exemple.createDimension().height));
    }

    // Bibliothèques tiers d’assertions :Hamcrest

    // Avec Hamcrest, on utilise la seul assertion assertThat
    // on précise la valeur réel et le matcher approprié qui spécifie la valeur
    // attendue

    // instanceOf -> tester le type
    @Test
    @Tag("HAMCREST")
    public void instancePersonneTest() {
        assertThat(Exemple.createPersonne(), Matchers.instanceOf(Personne.class));
    }

    // hasProperty -> tester les propriétés des JavaBeans
    @Test
    @Tag("HAMCREST")
    public void propertyPersonneTest() {
        assertThat(Exemple.createPersonne(), Matchers.hasProperty("prenom"));
        assertThat(Exemple.createPersonne(), Matchers.hasProperty("prenom", equalTo("yves")));
    }

    // samePropertyValuesAs -> tester si toutes les propriétés ont la même valeur
    // Le deuxième paramètre correspont aux attributs qui seront ignorés
    @Test
    @Tag("HAMCREST")
    public void propertyAllPersonneTest() {
        Personne p1 = new Personne("yves", "roulot", LocalDate.of(1900, 5, 14));
        // p1.setId(3L);
        assertThat(Exemple.createPersonne(), Matchers.samePropertyValuesAs(p1, "id", "dateNaissance"));
    }

    @Test
    @Tag("HAMCREST")
    public void stringTest() {
        assertThat(Exemple.createString(), Matchers.startsWith("He"));
    }

    @Test
    @Tag("HAMCREST")
    public void opLogicalTest() {
        assertThat(Exemple.createString(), Matchers.allOf(Matchers.startsWith("He"), Matchers.endsWith("ld")));
    }

    @Test
    @Tag("HAMCREST")
    public void asStringTest() {
        assertThat(Exemple.createPersonne().toString(),
                Matchers.hasToString(equalTo("Personne [id=3, prenom=yves, nom=roulot, dateNaissance=1988-05-14]")));

    }

    @Test
    @Tag("HAMCREST")
    public void listHamcrestTest() {
        assertThat(Exemple.processList(), Matchers.hasSize(4));
        assertThat(Exemple.processList(), Matchers.hasItem(6));
        assertThat(Exemple.processList(), Matchers.hasItems(6, 1, 23));
        assertThat(Exemple.processList(), Matchers.containsInAnyOrder(23, 1, 6, 4));
        assertThat(Exemple.processList(), Matchers.containsInRelativeOrder(1, 6, 4, 23));
        assertThat(Exemple.processList(), Matchers.everyItem(Matchers.lessThan(100)));
    }

    @Test
    @Tag("HAMCREST")
    public void mapTest() {
        assertThat(Exemple.processMap(), Matchers.hasKey(42));
        assertThat(Exemple.processMap(), Matchers.hasValue("azerty"));
        assertThat(Exemple.processMap(), Matchers.hasEntry(12, "azerty"));
    }

    @Test
    @Tag("HAMCREST")
    public void regexTest() {
        assertThat(Exemple.createString(), Matchers.matchesPattern("[a-zA-Z ]+"));
    }

    // AssertJ
    @Test
    public void listAssertJTest() {
        Assertions.assertThat(Exemple.processList()).isNotEmpty().as("La liste doit contenir 6 et 4").contains(6, 4)
                .size().isEqualTo(4);
        Assertions.assertThat(Exemple.processList()).filteredOn(elm -> elm < 15).containsExactly(1, 6, 4); // java 8
    }

    @Test
    public void stringAssertJTest() {
        Assertions.assertThat(Exemple.createString()).containsWhitespaces().containsAnyOf("W", "a", "y");
        Assertions.assertThat(Exemple.createString()).containsWhitespaces().contains("W", "o");
    }

    
    // JSONAssert
    @Test
    public void jsonObjectTest() {

        String expectedLenient = "{\"id\":5, \"prenom\":\"John\"}";
        String expectedStrict = "{\"id\":5, \"prenom\":\"John\" , \"nom\":\"Doe\", \"dateNaissance\"=[1991,11,9]}";
        try {
            JSONAssert.assertEquals(expectedLenient, Exemple.createJson(), JSONCompareMode.LENIENT); // LENIENT -> false
            JSONAssert.assertEquals(expectedStrict, Exemple.createJson(), JSONCompareMode.STRICT); // STRICT -> true
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void jsonArrayTest() {
        String expectedLenient = "[{\"id\":6, \"prenom\":\"Jane\" , \"nom\":\"Doe\", \"dateNaissance\"=[1996,9,11]}"
                + ",{\"id\":5, \"prenom\":\"John\" , \"nom\":\"Doe\", \"dateNaissance\"=[1991,11,9]}]";
        String expectedStrict = "[{\"id\":5, \"prenom\":\"John\" , \"nom\":\"Doe\", \"dateNaissance\"=[1991,11,9]}"
                + ",{\"id\":6, \"prenom\":\"Jane\" , \"nom\":\"Doe\", \"dateNaissance\"=[1996,9,11]}]";

        try {
            JSONAssert.assertEquals(expectedLenient, Exemple.createArrayJson(), JSONCompareMode.LENIENT);
            JSONAssert.assertEquals(expectedStrict, Exemple.createArrayJson(), JSONCompareMode.STRICT);
            JSONAssert.assertEquals("[2]", Exemple.createArrayJson(), new ArraySizeComparator(JSONCompareMode.STRICT));
        } catch (JsonProcessingException e) {

            e.printStackTrace();
        } catch (JSONException e) {

            e.printStackTrace();
        }
    }
    
    
    // Assumption
    @Test
    public void testAssumption() {
        assumeFalse(System.getenv("OS").startsWith("Windows"));
        fail();
    }
    
    @Test
    public void assumptionThatTest() {
        assumingThat(System.getenv("OS").startsWith("Windows"),
           ()->assertTrue(new File("c:/Windows").exists(),"Répertoire Windows inexistant"));
    }
    
    // Test répété
    @DisplayName("Un test répété")
  //  @RepeatedTest(value=4,name= "{displayName} {currentRepetition}/{totalRepetitions}")
    @RepeatedTest(value=4,name= RepeatedTest.LONG_DISPLAY_NAME)
    public void testRepete(RepetitionInfo info) {
        System.out.println(info.getCurrentRepetition() + " / "+info.getTotalRepetitions());
        assertEquals("Hello World",Exemple.createString());
    }
    
    // Test paramétré
    @ParameterizedTest
    @ValueSource(ints = {-4,-2,0,3,7})
    public void parametreTest(int param) {
        assertTrue(Exemple.interval(param, -4, 7));
    }
    
    @ParameterizedTest
    @EnumSource(value=Month.class,mode=Mode.EXCLUDE,names= {"FEBRUARY","APRIL","JUNE","SEPTEMBER","NOVEMBER"})
    public void enumParamTest(Month month) {
        assertEquals(LocalDate.of(2023,month,31).getDayOfMonth(),Exemple.lastDayOfMonth(month).getDayOfMonth());
    }
    
    @ParameterizedTest
    @CsvSource(delimiter = ';', value= {"1;3","3;6","43;2"})
    public void csvParamTest(int a, int b) {
        Calcul c=new Calcul();
        assertEquals(a+b,c.addition(a, b));
    }
    
    @ParameterizedTest
    @CsvFileSource(resources = "/add_data_value.csv", delimiter=';')
    public void csvFileParamTest(int a, int b) {
        Calcul c=new Calcul();
        assertEquals(a+b,c.addition(a, b));
    }
    
    @ParameterizedTest
    @MethodSource("providerData")
    public void methodParamTest(int i) {
        assertTrue(Exemple.interval(i, -4, 7));
    }
    
    public static Stream<Integer> providerData(){
        return Stream.of(-4,5,1,7);
    }
    
    // Test Dynamique
    @TestFactory
    Collection<DynamicTest> dynamicFactoryTest(){
        Calcul c=new Calcul();
        Collection<DynamicTest> dt=new ArrayList<>();
        for(int i=0; i<10;i+=2) {
            int j=i;
            dt.add(dynamicTest("addition: "+i,()-> assertEquals(j+j,c.addition(j,j))));
        }
        return dt;
    }
    
    @DisplayName("Exemple d'injection de paramètre")
    @Tag("info")
    @Tag("test")
    @Test
    public void exempleTestInfo(TestInfo info) {
        System.out.println(info.getDisplayName());
        System.out.println(info.getTestClass().get());
        System.out.println(info.getTestMethod().get());
        info.getTags().forEach(System.out::println);
    }
    
}
