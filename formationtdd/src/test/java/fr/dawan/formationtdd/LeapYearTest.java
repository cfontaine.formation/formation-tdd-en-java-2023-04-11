package fr.dawan.formationtdd;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeapYearTest {
    
    private LeapYear ly;
    
    @BeforeEach
    public void setup(){
        ly=new LeapYear();
    }
    
    @Test
    public void anneeNonBissextileTest() {
        assertFalse(ly.isLeapYear(2023));
    }
    
    @Test
    public void divisiblePar4Test() {
        assertTrue(ly.isLeapYear(2024));
    }
    
    @Test
    public void divisiblePar100Test() {
        assertFalse(ly.isLeapYear(1900));
    }
    
    @Test
    public void divisiblePar400Test() {
        assertTrue(ly.isLeapYear(2000));
    }
}
