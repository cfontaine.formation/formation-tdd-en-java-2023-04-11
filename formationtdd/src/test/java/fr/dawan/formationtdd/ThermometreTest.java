package fr.dawan.formationtdd;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ThermometreTest {
    
    private Thermometre thermometre;
    
    @BeforeEach
    public void setup() {
        thermometre=new Thermometre();
    }
    
    @Test
    public void pasDeTemperatureTest() throws Exception {
        assertEquals(0, thermometre.nearestZero());
    }
    
    @Test 
    public void max100TemperatureTest() {
        int temp[]=new int[101];
        assertThrows(Exception.class, () -> thermometre.nearestZero(temp) );
    }
    
    @Test
    public void nearest0temperatureTest() throws Exception {
        assertEquals(2, thermometre.nearestZero(2, 4, 6, -7, -3));
        assertEquals(-2, thermometre.nearestZero(-3, 5, 7, -2, 4));
    }

    @Test
    public void nearestZeroTempPlusTest() throws Exception {
        assertEquals(2, thermometre.nearestZero(2, 4, 6, -7, -2));
    }

}
