package fr.dawan.formationtdd.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.dawan.formationtdd.beans.Article;
import fr.dawan.formationtdd.repository.ArticleRepository;

@ExtendWith(MockitoExtension.class)
public class ArticleServiceTest {

    @Mock
    private ArticleRepository repository;

    @InjectMocks
    private ArticleService service;

    // private AutoCloseable close;// Méthode 2

//    @BeforeEach
//    public void setup() {
    // repository= mock(ArticleRepository.class); // Méthode 1

    // close= MockitoAnnotations.openMocks(this); // Méthode 2
    // service=new ArticleService(repository); // Méthode 3
//    }
//    
//    @AfterEach
//    public void tearDown() throws Exception {   // Méthode 2
//        close.close();
//    }

    @Test
    public void findByIdTest() throws Exception {
        Article a = new Article("Souris", 19.0);
        a.setId(1L);
        when(repository.findById(1L)).thenReturn(a);
        MatcherAssert.assertThat(service.findById(1L), Matchers.samePropertyValuesAs(a));
    }

    @Test
    public void findByIdExceptionTest() throws Exception {
        when(repository.findById(100L)).thenThrow(Exception.class);
        assertThrows(Exception.class,()-> service.findById(100L));
    }

    @Test
    public void checkTest() {
        try (MockedStatic<ArticleService> servStat = Mockito.mockStatic(ArticleService.class)) {
            servStat.when(() -> ArticleService.check()).thenReturn(new Article());
            MatcherAssert.assertThat(ArticleService.check(), Matchers.samePropertyValuesAs(new Article()));
        }
    }

    // Méthode void
    @Test
    public void deleteTest() throws Exception {
        doThrow(Exception.class).when(repository).delete(100L);
        assertThrows(Exception.class, () -> service.delete(100L));
    }

    @Test
    public void findById2() throws Exception {
        Article a = new Article("tv", 600.0);
        a.setId(5L);
        when(repository.findById(anyLong())).thenReturn(a);
        MatcherAssert.assertThat(service.findById(5L), Matchers.samePropertyValuesAs(a));
        MatcherAssert.assertThat(service.findById(150L), Matchers.samePropertyValuesAs(a));
    }
    
    @Test
    public void verifyTest() throws Exception {
        Article a = new Article("tv", 600.0);
        a.setId(5L);
        when(repository.findById(anyLong())).thenReturn(a);
        service.findById(1L);
        service.findById(1L);
        service.findById(1L);
        verify(repository,times(3)).findById(1L);
        verify(repository,never()).findAll();
     //   verify(repository,only()).findById(1L);
    }
    
    @Test
    public void spyTest() throws Exception {
        Article a = new Article("Souris", 19.0);
        a.setId(1L);
        ArticleRepository repo=new ArticleRepository();
        ArticleRepository spyRepo=spy(repo);
        ArticleService serv=new ArticleService(spyRepo);
        when(spyRepo.findById(1L)).thenReturn(a);
        assertEquals(3,serv.findAll().size()); // méthode réel
        MatcherAssert.assertThat(serv.findById(1L), Matchers.samePropertyValuesAs(a));
    }

}
