package fr.dawan.formationtdd.suites;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

import fr.dawan.formationtdd.FizzBuzzTest;
import fr.dawan.formationtdd.LeapYearTest;
import fr.dawan.formationtdd.ThermometreTest;

@Suite
@SelectClasses({FizzBuzzTest.class,ThermometreTest.class,LeapYearTest.class})
public class SuiteTestClass {

}
