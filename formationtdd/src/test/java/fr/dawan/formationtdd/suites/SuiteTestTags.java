package fr.dawan.formationtdd.suites;

import org.junit.platform.suite.api.IncludeTags;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;

@Suite
@SelectPackages("fr.dawan.formationtdd")
@IncludeTags("HAMCREST")
public class SuiteTestTags {

}
